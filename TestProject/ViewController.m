//
//  ViewController.m
//  TestProject
//
//  Created by 曹诚飞 on 2018/11/29.
//  Copyright © 2018 曹诚飞. All rights reserved.
//

#import "ViewController.h"
#import "AViewController.h"

@interface ViewController ()

@property (nonatomic, strong) UIButton *pushAControllerButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.view addSubview:self.pushAControllerButton];
    [self layoutSubViews];
}

#pragma mark - Actions
- (void)actionToPushAViewController {
    AViewController *viewController = [[AViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
    
}

#pragma mark - layout
- (void)layoutSubViews {
    [self.pushAControllerButton sizeToFit];
    self.pushAControllerButton.center = self.view.center;
}

#pragma mark - lazy
- (UIButton *)pushAControllerButton {
    if (!_pushAControllerButton) {
        _pushAControllerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pushAControllerButton setTitle:@"push to A ViewController" forState:UIControlStateNormal];
        [_pushAControllerButton setTitleColor:UIColor.orangeColor forState:UIControlStateNormal];
        [_pushAControllerButton addTarget:self action:@selector(actionToPushAViewController) forControlEvents:UIControlEventTouchUpInside];
    }
    return _pushAControllerButton;
}


@end
