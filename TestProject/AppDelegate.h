//
//  AppDelegate.h
//  TestProject
//
//  Created by 曹诚飞 on 2018/11/29.
//  Copyright © 2018 曹诚飞. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

