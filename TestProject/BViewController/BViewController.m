//
//  BViewController.m
//  TestProject
//
//  Created by 曹诚飞 on 2018/11/29.
//  Copyright © 2018 曹诚飞. All rights reserved.
//

#import "BViewController.h"

@interface BViewController ()
@property (nonatomic, strong) UILabel *contentLabel;
@end

@implementation BViewController

- (instancetype)initWithContent:(NSString *)content {
    self = [super init];
    if (self) {
        self.contentLabel.text = content;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.contentLabel];
    [self layoutSubViews];
}

#pragma mark - layout
- (void)layoutSubViews {
    [self.contentLabel sizeToFit];
    self.contentLabel.center = self.view.center;
}

#pragma mark - lazy
- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.textColor = UIColor.orangeColor;
    }
    return _contentLabel;
}

@end
