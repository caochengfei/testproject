//
//  BViewController.h
//  TestProject
//
//  Created by 曹诚飞 on 2018/11/29.
//  Copyright © 2018 曹诚飞. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BViewController : UIViewController

- (instancetype)initWithContent:(NSString *)content;

@end

NS_ASSUME_NONNULL_END
