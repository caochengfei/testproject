//
//  main.m
//  TestProject
//
//  Created by 曹诚飞 on 2018/11/29.
//  Copyright © 2018 曹诚飞. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
